﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Web.Models.Request
{
    public class UpdateEmailRequest
    {
        public string Email { get; set; }
    }
}
