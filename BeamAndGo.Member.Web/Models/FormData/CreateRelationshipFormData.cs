﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Web.Models.FormData
{
    public class CreateRelationshipFormData
    {
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please enter first name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please enter last name")]
        public string LastName { get; set; }

        [Display(Name = "Birthdate")]
        public string BirthDate { get; set; }

        [Display(Name = "Gender")]
        public char? Gender { get; set; }

        [Display(Name = "Calling Code")]
        [Required(ErrorMessage = "Invalid calling code")]
        public string CallingCode { get; set; }

        [Display(Name = "Mobile Number")]
        [Required(ErrorMessage = "Please enter a numeric mobile number")]
        public string MobileNumber { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        [Display(Name = "Country")]
        public string CountryCode { get; set; }

        [Display(Name = "Relationship")]
        [Required(ErrorMessage = "Please select relationship")]
        public string RelationshipTypeSId { get; set; }
        
    }
}
