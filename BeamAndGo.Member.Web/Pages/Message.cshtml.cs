using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BeamAndGo.Member.Web.Pages
{
    public class MessageModel : PageModel
    {
        public string Message { get; set; }

        public IActionResult OnGet([FromQuery(Name = "msg")] string msg = "msg")
        {
            Message = msg;
            return Page();
        }
    }
}
