using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Member.Web.Models;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace BeamAndGo.Member.Web.Pages
{
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class ErrorModel : PageModel
    {
        public string RequestId { get; set; }
        public string Error { get; set; }
        public string ErrorDescription { get; set; }
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

        private readonly ILogger<ErrorModel> _logger;

        public ErrorModel(ILogger<ErrorModel> logger)
        {
            _logger = logger;
        }

        public IActionResult OnGet([FromQuery(Name = "error")] string error = "error", [FromQuery(Name = "error_description")] string error_description = "An error occurred")
        {
            RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            Error = error;
            ErrorDescription = error_description;
            Response.StatusCode = 500;

            // Retrieve error if this page was called from an exception handler
            var exceptionHandler = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionHandler != null)
            {
                _logger.LogError(exceptionHandler.Error, exceptionHandler.Error.Message);
            }
            else
            {
                if (Error == ErrorMessage.AccessDenied.Error)
                {
                    var sid = HttpContext.Session.GetString(SessionKey.MemberSid);

                    _logger.LogWarning("member {@sid} does not have permission to access Member Web", sid);
                }
                else
                {
                    _logger.LogError(Error, ErrorDescription);
                }
            }

            return Page();
        }
    }
}
