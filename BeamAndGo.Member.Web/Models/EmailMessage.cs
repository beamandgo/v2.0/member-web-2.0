﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Web.Models
{
    public class EmailMessage
    {
        public static EmailMessage EmailVerified  = new EmailMessage { Message = "Your email has been successfully verified." };
        public static EmailMessage EmailVerifiedFailed = new EmailMessage { Message = "Sorry, we were unable to verify your email due to a system error. Please try again" };


        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
