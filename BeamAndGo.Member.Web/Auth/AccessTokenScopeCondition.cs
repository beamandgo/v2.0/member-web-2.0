﻿namespace BeamAndGo.Member.Web.Auth
{
    public enum AccessTokenScopeCondition
    {
        All = 1,
        Any = 2
    }
}
