﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Web.Models
{
    public class MemberAddressListModel
    {
        public IList<MemberAddressModel> MemberAddresses { get; set; } = new List<MemberAddressModel>();

        public MemberAddressListModel()
        {

        }

        public MemberAddressListModel(IList<MemberAddress> addresses)
        {
            foreach (var address in addresses)
                MemberAddresses.Add(new MemberAddressModel(address));
        }
    }
}
