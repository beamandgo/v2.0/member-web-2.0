using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Member.Web.Filters;
using BeamAndGo.Member.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BeamAndGo.Member.Web.Pages
{
    [LoginRequired]
    public class CreateNewRelationshipModel : PageModel
    {
        private readonly ApplicationOptions _applicationOptions;
        private readonly ILogger<CreateNewRelationshipModel> _logger;
        private readonly IMemberService _memberService;
        private readonly IMemberRelationshipService _memberRelationshipService;
        private readonly IMemberRelationshipTypeService _memberRelationshipTypeService;


        public bool IsXHR { get { return Request.Headers["X-Requested-With"] == "XMLHttpRequest"; } }
        public IEnumerable<MemberRelationshipType> RelationshipTypes { get; private set; }
        public string MaxBirthdate { get; set; }
        public string MinBirthdate { get; set; }
        public string MemberCountryCode { get; set; }
        public string StaticAssetsUrl { get; set; }


        [BindProperty]
        public CreateMemberRelationshipData FormData { get; set; }

        [BindProperty]
        public string RelationshipBirthdate { get; set; }
        [BindProperty]
        public string RelationshipType { get; set; }


        public CreateNewRelationshipModel(ILogger<CreateNewRelationshipModel> logger, 
                                          IMemberRelationshipService memberRelationshipService, 
                                          IMemberRelationshipTypeService memberRelationshipTypeService,
                                          IMemberService memberService,
                                          IOptions<ApplicationOptions> applicationOptions)
        {
            _logger = logger;
            _memberRelationshipService = memberRelationshipService;
            _memberRelationshipTypeService = memberRelationshipTypeService;
            _memberService = memberService;
            _applicationOptions = applicationOptions.Value;
            FormData = new CreateMemberRelationshipData();
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var sid = HttpContext.Session.GetString(SessionKey.MemberSid);

            try
            {
                MinBirthdate = DateTime.Now.AddYears(-100).ToString("yyyy-MM-dd");
                MaxBirthdate = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
                StaticAssetsUrl = _applicationOptions.StaticAssetsUrl;

                RelationshipTypes = await _memberRelationshipTypeService.ListAllActiveSortedByName();

                var member = await _memberService.GetBySid(sid);
                MemberCountryCode = member?.CountryCode;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostCreateAsync()
        {
            if (ModelState.IsValid)
            {
                try
                {
                    FormData.MemberSid = HttpContext.Session.GetString(SessionKey.MemberSid);

                    var birthDate = Convert.ToDateTime(RelationshipBirthdate);
                    if (birthDate.Year > 1)
                    {
                        FormData.BirthDay = birthDate.Day;
                        FormData.BirthMonth = birthDate.Month;
                        FormData.BirthYear = birthDate.Year;
                    }

                    if (RelationshipType != null)
                    {
                        FormData.RelationshipTypeId = Convert.ToInt32(RelationshipType.Split("|")[0]);
                    }

                    var relationship = await _memberRelationshipService.Create(FormData);

                    return new OkObjectResult("Successfully added new relationship");
                }
                catch (ArgumentException ex)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
                }
                catch (Exception ex)
                {
                    if (ex is InvalidOperationException)
                    {
                        _logger.LogInformation(ex, ex.Message);
                        return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                    }
                    else
                    {
                        _logger.LogError(ex, ex.Message);
                        return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                    }                
                }
            }
            else
            {
                var message = string.Join("\n", ModelState.Values
                                    .SelectMany(v => v.Errors)
                                    .Select(e => e.ErrorMessage));

                return BadRequest(message);
            }
        }
    }
}
