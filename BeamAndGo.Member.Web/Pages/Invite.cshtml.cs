using System;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;
using BeamAndGo.Member.Invite;
using BeamAndGo.Member.Web.Filters;
using BeamAndGo.Member.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;


namespace BeamAndGo.Member.Web.Pages
{
    [LoginRequired]
    public class InviteModel : PageModel
    {
        private readonly ILogger<InviteModel> _logger;
        private readonly IMemberService _memberService;
        private readonly IMemberEmailInviteService _memberEmailInviteService;
        private readonly IMemberPhoneInviteService _memberPhoneInviteService;
        private readonly ApplicationOptions _applicationOptions;

        [BindProperty]
        public string CallingCode { get; set; }

        [BindProperty]
        public string MobileNumber { get; set; }

        [BindProperty]
        public string Email { get; set; }

        [BindProperty]
        public string MemberCountryCode { get; set; }

        [BindProperty]
        public string StaticAssetsUrl { get; set; }

        public bool IsXHR { get { return Request.Headers["X-Requested-With"] == "XMLHttpRequest"; } }

        public InviteModel(
            ILogger<InviteModel> logger,
            IMemberEmailInviteService memberEmailInviteService,
            IMemberPhoneInviteService memberPhoneInviteService,
            IMemberService memberService,
            IOptions<ApplicationOptions> applicationOptions)
        {
            _logger = logger;
            _memberEmailInviteService = memberEmailInviteService;
            _memberPhoneInviteService = memberPhoneInviteService;
            _memberService = memberService;
            _applicationOptions = applicationOptions.Value;
        }


        public async Task<IActionResult> OnGetAsync()
        {
            var sid = HttpContext.Session.GetString(SessionKey.MemberSid);

            try
            {
                var member = await _memberService.GetBySid(sid);
                MemberCountryCode = member?.CountryCode;
                StaticAssetsUrl = _applicationOptions.StaticAssetsUrl;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }

        public async Task<IActionResult> OnPostInviteSMSAsync()
        {
            var sid = HttpContext.Session.GetString(SessionKey.MemberSid);
            try
            {
                MobileNumber = MobileNumber.SanitizePhone();
                CallingCode = CallingCode.SanitizePhone().Replace("+", "");

                var referralLink = _applicationOptions.MemberWebUrl + _applicationOptions.ReferralSignUpUrl;

                await _memberPhoneInviteService.Send(sid, CallingCode, MobileNumber, referralLink);

                return new OkObjectResult($"Way to go! We have sent an invitation to +{CallingCode} {MobileNumber} which should arrive shortly!");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }

        public async Task<IActionResult> OnPostInviteEmailAsync()
        {
            var sid = HttpContext.Session.GetString(SessionKey.MemberSid);
            try
            {
                var referralLink = _applicationOptions.MemberWebUrl + _applicationOptions.ReferralSignUpUrl;

                await _memberEmailInviteService.Send(sid, Email, referralLink);
                return new OkObjectResult($"Way to go! We have sent an invitation to {Email} which should arrive shortly!");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }
    }
}
