﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Web.Models.Request
{
    public class CreatePhoneRequest
    {
        public string CallingCode { get; set; }
        public string PhoneNumber { get; set; }
    }
}
