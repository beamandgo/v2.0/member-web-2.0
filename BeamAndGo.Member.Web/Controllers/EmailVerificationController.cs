﻿using BeamAndGo.Core.Common;
using BeamAndGo.Member.Verification;
using BeamAndGo.Member.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Web.Controllers
{
    public class EmailVerificationController : Controller
    {
        private readonly IMemberEmailVerificationService _memberEmailVerificationService;
        private readonly ILogger _logger;

        public EmailVerificationController(ILogger<EmailVerificationController> logger, IMemberEmailVerificationService memberEmailVerificationService)
        {
            _memberEmailVerificationService = memberEmailVerificationService;
            _logger = logger;
        }

        [HttpGet]
        [Route("/verify")]
        public async Task<IActionResult> VerifyEmail([FromQuery(Name = "code")] string code)
        {
            try
            {
                await _memberEmailVerificationService.Verify(code);

                return Redirect($"/Message?msg={EmailMessage.EmailVerified.Message}");
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex.Message);
                    return Redirect($"/Message?msg={ExceptionExtensions.GetDescription(ex)}");
                }
                else
                {
                    _logger.LogError(ex.Message, ex);
                    return Redirect($"/Message?msg={EmailMessage.EmailVerifiedFailed.Message}");
                }
            }
        }
    }
}
