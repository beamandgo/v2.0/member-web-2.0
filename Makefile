# Configure this for every project
PROJECT = BeamAndGo.Member.Web
CONFIGURATION = Release

# GitLab Configuration
CI_PROJECT_PATH = beamandgo/v2.0/member-web-2.0
CI_PROJECT_ID = 24710272
CI_GROUP_ID = 10550515
CI_SERVER_URL = https://www.gitlab.com
CI_REGISTRY = registry.gitlab.com

# Reasonable defaults for project directory and files
# Update only if the project deviates from the standard
PROJECT_DIR := $(CURDIR)/$(PROJECT)
PROJECT_CSPROJ := $(PROJECT).csproj
TEST_DIR := $(CURDIR)/$(PROJECT).Test
TEST_CSPROJ := $(PROJECT).Test.csproj
ARTIFACT_DIR := $(CURDIR)/artifacts

# Do not touch - this generates a version from git commit tag/hash
VERSION := $(shell git describe)

build: check_version check_token
	docker build -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:${VERSION} \
		--build-arg CI_PROJECT_ID=${CI_PROJECT_ID} \
		--build-arg CI_GROUP_ID=${CI_GROUP_ID} \
		--build-arg CI_SERVER_URL=${CI_SERVER_URL} \
		--build-arg CI_USERNAME=${CI_USERNAME} \
		--build-arg CI_TOKEN=${CI_TOKEN} \
		.

push: check_version check_token
	docker login ${CI_REGISTRY} -u ${CI_USERNAME} -p ${CI_TOKEN}
	docker push ${CI_REGISTRY}/${CI_PROJECT_PATH}:${VERSION} 
	docker logout ${CI_REGISTRY}

check_version:
ifndef VERSION
$(error No git tags to provide a version for build, please add an initial tag)
endif

check_token:
ifndef CI_USERNAME
$(error No CI_USERNAME defined, please provide a username)
endif
ifndef CI_TOKEN
$(error No CI_TOKEN defined, please provide a token/password)
endif