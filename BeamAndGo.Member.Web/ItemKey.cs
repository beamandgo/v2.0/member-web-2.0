﻿using System;
namespace BeamAndGo.Member.Web
{
    public static class ItemKey
    {
        public const string Accounts = "Accounts";

        public class Member
        {
            public const string Profile = "Member.Profile";
            public const string Country = "Member.Country";
        }
    }
}
