FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

ARG CI_SERVER_URL
ARG CI_GROUP_ID
ARG CI_USERNAME
ARG CI_TOKEN

# Copy csproj and restore as distinct layers
COPY BeamAndGo.Member.Web/*.csproj ./
RUN dotnet nuget add source \
	"$CI_SERVER_URL/api/v4/groups/$CI_GROUP_ID/-/packages/nuget/index.json" \
	--name group \
	--username $CI_USERNAME \
	--password $CI_TOKEN \
	--store-password-in-clear-text
RUN dotnet restore

# Copy everything else and build
COPY BeamAndGo.Member.Web ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "BeamAndGo.Member.Web.dll"]
