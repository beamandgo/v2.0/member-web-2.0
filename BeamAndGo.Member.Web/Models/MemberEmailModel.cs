﻿using System;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member.Web.Models
{
    public class MemberEmailModel
    {
        public int Id { get; set; }
        public string Sid { get; set; }
        public string Identifier { get; set; }
        public string Email { get; set; }
        public bool IsDefault { get; set; }
        public bool IsVerified { get; set; }
        public string VerificationMeta { get; set; }
        public DateTimeOffset? VerifiedAt { get; set; }
        public MemberEmailModel() { }

        public MemberEmailModel(MemberEmail memberEmail)
        {
            Id = memberEmail.Id;
            Sid = memberEmail.Sid;
            Identifier = memberEmail.Identifier;
            Email = memberEmail.Email;
            IsDefault = memberEmail.IsDefault;
            IsVerified = memberEmail.IsVerified;
            VerificationMeta = memberEmail.VerificationMeta;
            VerifiedAt = memberEmail.VerifiedAt;
        }
    }
    

}
