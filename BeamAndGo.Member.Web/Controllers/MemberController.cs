﻿using BeamAndGo.Member.Verification;
using BeamAndGo.Member.Web.Auth;
using BeamAndGo.Member.Web.Filters;
using BeamAndGo.Member.Web.Models;
using BeamAndGo.Member.Web.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Web.Controllers
{
    public partial class MemberController : Controller
    {
        private readonly IMemberService _memberService;
        private readonly IMemberAddressService _memberAddressService;
        private readonly IMemberEmailService _memberEmailService;
        private readonly IMemberEmailVerificationService _memberEmailVerificationService;
        private readonly IMemberPhoneService _memberPhoneService;
        private readonly IMemberPhoneVerificationService _memberPhoneVerificationService;
        private readonly IMemberRelationshipService _memberRelationshipService;
        private readonly ILogger _logger;

        public MemberController(ILogger<MemberController> logger,
                                IMemberService memberService, 
                                IMemberAddressService memberAddressService, 
                                IMemberEmailService memberEmailService,
                                IMemberEmailVerificationService memberEmailVerificationService,
                                IMemberPhoneService memberPhoneService, 
                                IMemberPhoneVerificationService memberPhoneVerificationService,
                                IMemberRelationshipService memberRelationshipService)
        {
            _memberService = memberService;
            _memberAddressService = memberAddressService;
            _memberEmailService = memberEmailService;
            _memberEmailVerificationService = memberEmailVerificationService;
            _memberPhoneService = memberPhoneService;
            _memberPhoneVerificationService = memberPhoneVerificationService;
            _memberRelationshipService = memberRelationshipService;
            _logger = logger;
        }

        [AccessTokenRequired("member")]
        [HttpGet]
        [Route("/api/member/{memberSid}")]
        public async Task<IActionResult> Get(string memberSid)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if(token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.GetBySid(memberSid);

                    return Ok(new MemberModel(member));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (MemberServiceException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.MemberServiceError.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }

        [AccessTokenRequired("member")]
        [HttpPut]
        [Route("/api/member/{memberSid}")]
        public async Task<IActionResult> Put(string memberSid, [FromBody] UpdateMemberData request)
        {
            try
            {
                var token = (AccessToken)HttpContext.Items["AccessToken"];
                var userSid = token.Subject;

                if (token.Scope.Contains("admin") || userSid == memberSid)
                {
                    var member = await _memberService.Update(memberSid, request);

                    return Ok(new MemberModel(member));
                }

                return BadRequest(ApiErrorMessage.ScopeNotGranted);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.InvalidRequest.Code, Message = ex.Message });
            }
            catch (MemberServiceException ex)
            {
                return BadRequest(new ApiErrorMessage { Code = ApiErrorMessage.MemberServiceError.Code, Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return StatusCode(StatusCodes.Status500InternalServerError, ApiErrorMessage.ServerError);
            }
        }       
    }
}
