﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http.Extensions;

namespace BeamAndGo.Member.Web.Filters
{
    public class LoginRequiredAttribute : Attribute, IAsyncResourceFilter
    {
        public LoginRequiredAttribute()
        {
        }

        public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
        {
            if (!context.HttpContext.Session.IsAvailable || string.IsNullOrEmpty(context.HttpContext.Session.GetString(SessionKey.MemberSid)))
            {
                var currentUrl = UriHelper.GetDisplayUrl(context.HttpContext.Request);

                context.HttpContext.Session.SetString(SessionKey.RedirectUrl, currentUrl);

                context.HttpContext.Response.Redirect("/login");
                return;
            }

            var memberService = context.HttpContext.RequestServices.GetService<IMemberService>();
            //var locationService = context.HttpContext.RequestServices.GetService<ILocationService>();

            var memberSid = context.HttpContext.Session.GetString(SessionKey.MemberSid);
            var member = await memberService.GetBySid(memberSid);
            //var country = await locationService.GetCountryByCode(member.CountryCode);

            context.HttpContext.Items.Add(ItemKey.Member.Profile, member);
            //context.HttpContext.Items.Add(ItemKey.Member.Country, country);

            await next();
        }
    }
}
