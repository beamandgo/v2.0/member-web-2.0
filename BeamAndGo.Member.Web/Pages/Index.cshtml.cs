﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Member.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using BeamAndGo.Core.Common;
using BeamAndGo.Member.Web.Models;
using BeamAndGo.Core.Country;

namespace BeamAndGo.Member.Web.Pages
{
    [LoginRequired]
    [ValidateAntiForgeryToken]
    public class IndexModel : PageModel
    {
        private readonly ICountryService _countryService;
        private readonly ILogger<IndexModel> _logger;
        private readonly IMemberService _memberService;
        private readonly IMemberRelationshipService _memberRelationshipService;
        private readonly IMemberRelationshipTypeService _memberRelationshipTypeService;
        private readonly ApplicationOptions _applicationOptions;


        public bool HasRelationships { get; set; }
        public bool KYCEnabled { get; set; } = false;
        public bool KYCVerified { get; set; } = false;
        public IAsyncEnumerable<MemberRelationship> Relationships { get; private set; }
        public IEnumerable<MemberRelationshipType> RelationshipTypes { get; private set; }
        public MemberAddress Address { get; set; }
        public MemberEmail Email { get; set; }
        public MemberPhone Phone { get; set; }
        public string MaxBirthdate { get; set; }
        public string MinBirthdate { get; set; }
        public string ReferralCode { get; set; }
        public string CountryName { get; set; }

        [BindProperty]
        public string Birthdate { get; set; }
        [BindProperty]
        public string RelationshipBirthdate { get; set; }
        [BindProperty]
        public string RelationshipType { get; set; }
        [BindProperty]
        public string SelectedRelationshipSid { get; set; }
        [BindProperty]
        public UpdateMemberData FormData { get; set; }
        [BindProperty]
        public UpdateMemberRelationshipData RelationshipFormData { get; set; }
        [BindProperty]
        public bool ShowReturnBtn { get; set; }
        [BindProperty]
        public string ReturnUrl { get; set; }

        public IndexModel(ILogger<IndexModel> logger,
                          IMemberRelationshipService memberRelationshipService,
                          IMemberRelationshipTypeService memberRelationshipTypeService,
                          IMemberService memberService,
                          ICountryService countryService,
                          IOptions<ApplicationOptions> applicationOptions)
        {
            _logger = logger;
            _countryService = countryService;
            _memberService = memberService;
            _memberRelationshipService = memberRelationshipService;
            _memberRelationshipTypeService = memberRelationshipTypeService;

            FormData = new UpdateMemberData();
            RelationshipFormData = new UpdateMemberRelationshipData();

            Address = new MemberAddress();
            Phone = new MemberPhone();
            Email = new MemberEmail();

            _applicationOptions = applicationOptions.Value;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var accessToken = HttpContext.Session.GetString(SessionKey.Token.AccessToken);
            var sid = HttpContext.Session.GetString(SessionKey.MemberSid);

            try
            {
                var member = await _memberService.GetBySid(sid, true);
                var country = await _countryService.GetByAlpha2Code(member.CountryCode);
                ReferralCode = member.ReferralCode;
                FormData.FirstName = member.FirstName;
                FormData.LastName = member.LastName;
                FormData.MiddleName = string.IsNullOrEmpty(member.MiddleName) ? "" : member.MiddleName;
                FormData.DisplayName = string.IsNullOrEmpty(member.DisplayName) ? "" : member.DisplayName;
                FormData.CountryCode = member.CountryCode;
                FormData.Gender = member.Gender;

                CountryName = country.Name;
                KYCEnabled = _applicationOptions.KYCEnabled;
                MinBirthdate = DateTime.Now.AddYears(-100).ToString("yyyy-MM-dd");
                MaxBirthdate = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");

                if (member.BirthDay == null || member.BirthMonth == null || member.BirthYear == null)
                {
                    Birthdate = string.Empty;
                }
                else
                {
                    Birthdate = new DateTime((int)member.BirthYear, (int)member.BirthMonth, (int)member.BirthDay).ToString("yyyy-MM-dd");
                }

                if (member.Phones != null)
                {
                    Phone = member.Phones.Select(x => x).FirstOrDefault(x => x.MemberId == member.Id && x.IsDefault);
                }

                if (member.Emails != null)
                {
                    Email = member.Emails.Select(x => x).FirstOrDefault(x => x.MemberId == member.Id && x.IsDefault);
                }

                if (member.Addresses != null)
                {
                    Address = member.Addresses.Select(x => x).FirstOrDefault(x => x.MemberId == member.Id && x.IsDefault);
                }

                if (Phone != null && Email != null && Address != null)
                {
                    KYCVerified = (Phone.IsVerified && Email.IsVerified && Address.IsVerified);
                }

                Relationships = _memberRelationshipService.ListByMemberSid(member.Sid);
                RelationshipTypes = await _memberRelationshipTypeService.ListAllActiveSortedByName();
                HasRelationships = await Relationships.AnyAsync().ConfigureAwait(false);

                var sourceUrl = Request.Headers["Referer"].ToString();
                if (string.IsNullOrEmpty(sourceUrl))
                {
                    ShowReturnBtn = false;
                }
                else
                {
                    ShowReturnBtn = true;

                    if (sourceUrl.ToLower().Contains("pos"))
                    {
                        ReturnUrl = _applicationOptions.POSWebUrl;
                    }
                    else if (sourceUrl.ToLower().Contains("wallet"))
                    {
                        ReturnUrl = _applicationOptions.WalletWebUrl;
                    }else
                    {
                        ReturnUrl = _applicationOptions.StoreWebUrl;
                    }
                }

            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return RedirectToPage("/Error", new { error = ExceptionExtensions.GetCode(ex), error_description = ExceptionExtensions.GetDescription(ex) });
                }
                else
                {
                    throw;
                }
            }

            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var member = HttpContext.Items[ItemKey.Member.Profile] as Member;

                try
                {
                    var birthDate = Convert.ToDateTime(Birthdate);

                    if (birthDate.Year > 1)
                    {
                        FormData.BirthDay = birthDate.Day;
                        FormData.BirthMonth = birthDate.Month;
                        FormData.BirthYear = birthDate.Year;
                    }

                    await _memberService.Update(member.Sid, FormData);

                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully updated your profile");

                    member.FirstName = FormData.FirstName;
                    member.LastName = FormData.LastName;

                    HttpContext.Items[ItemKey.Member.Profile] = member;
                }
                catch (ArgumentException ex)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
                }
                catch (Exception ex)
                {
                    if (ex is InvalidOperationException)
                    {
                        _logger.LogInformation(ex, ex.Message);
                        HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                    }
                    else
                    {
                        _logger.LogError(ex, ex.Message);
                        HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                    }
                }
            }

            return RedirectToPage("./Index");
        }
        public async Task<IActionResult> OnPostRemoveRelationship()
        {
            try
            {
                await _memberRelationshipService.Delete(SelectedRelationshipSid);

                HttpContext.Session.SetString(SessionKey.NotificationMessage.Sucess, "Successfully removed your relationship");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, $"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    HttpContext.Session.SetString(SessionKey.NotificationMessage.Error, ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }

            return RedirectToPage("./Index");
        }

        public async Task<IActionResult> OnPostUpdateRelationshipAsync()
        {
            try
            {
                var birthDate = Convert.ToDateTime(RelationshipBirthdate);

                if (birthDate.Year > 1)
                {
                    RelationshipFormData.BirthDay = birthDate.Day;
                    RelationshipFormData.BirthMonth = birthDate.Month;
                    RelationshipFormData.BirthYear = birthDate.Year;
                }

                if (RelationshipType != null)
                {
                    RelationshipFormData.RelationshipTypeId = Convert.ToInt32(RelationshipType.Split("|")[0]);
                }

                var relationship = await _memberRelationshipService.Update(SelectedRelationshipSid, RelationshipFormData);

                return new OkObjectResult("Successfully updated your relationship");
            }
            catch (ArgumentException ex)
            {
                _logger.LogInformation(ex, ex.Message);
                return BadRequest($"{ExceptionExtensions.GetDescription(ex)}");
            }
            catch (Exception ex)
            {
                if (ex is InvalidOperationException)
                {
                    _logger.LogInformation(ex, ex.Message);
                    return BadRequest($"{ExceptionExtensions.GetDescription(ex)} {ErrorMessage.ErrorMessagePostfix.ErrorDescription}");
                }
                else
                {
                    _logger.LogError(ex, ex.Message);
                    return BadRequest(ErrorMessage.ErrorMessageDefault.ErrorDescription);
                }
            }
        }
    }
}
